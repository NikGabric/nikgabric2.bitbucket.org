
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";



/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = getSessionId();
  //alert("dela");
  //dodajMeritveVitalnihZnakov(stPacienta);

    var ime = " ";
    var priimek = " ";
    var datumRojstva = " ";

    switch(stPacienta){
        case 1:
	        ime = "Marko";
    	    priimek = "Markovic";
            datumRojstva = "1975-01-01";
            break;
        case 2:
	        ime = "Milena";
    	    priimek = "Mohoric";
            datumRojstva = "1955-05-03";
            break;
        case 3:
            ime = "Miha";
    	    priimek = "Mraz";
            datumRojstva = "2012-02-12";
            break;
        }

var ehrId = getSessionId();
var datumInUra = "";
var telesnaVisina = "";
var telesnaTeza = "";
var telesnaTemperatura = "";
var sistolicniKrvniTlak = "";
var diastolicniKrvniTlak = "";
var nasicenostKrviSKisikom = "";
var merilec = "";
    
    
    switch(stPacienta){
    	case 1:
	    	datumInUra = "2018-03-02T14:30Z";
    		telesnaVisina = "190";
	    	telesnaTeza = "84.5";
    		telesnaTemperatura = "36.35";
    		sistolicniKrvniTlak = "120";
    	    diastolicniKrvniTlak = "88";
    		nasicenostKrviSKisikom = "95";
    		merilec = "Medicinski brat Milos Matko";
    	    break;
    	case 2:
    		datumInUra = "2018-04-11T22:10Z";
    		telesnaVisina = "155";
    		telesnaTeza = "53";
    		telesnaTemperatura = "36.1";
    		sistolicniKrvniTlak = "95";
    	    diastolicniKrvniTlak = "70";
    		nasicenostKrviSKisikom = "85";
    		merilec = "Medicinska sestra Mateja Miklavcic";
    		break;
    	case 3:
    		datumInUra = "2018-01-07T08:40Z";
    		telesnaVisina = "100";
    		telesnaTeza = "20.5";
    		telesnaTemperatura = "36.8";
    		sistolicniKrvniTlak = "135";
    		diastolicniKrvniTlak = "98";
    		nasicenostKrviSKisikom = "98";
    		merilec = "Medicinska sestra Manja Majdic";
    	    break;	
    }

		$.ajaxSetup({
		    headers: {"Ehr-Session": ehrId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                $("#preberiPredlogoBolnika").append('<option value="'+ime+','+priimek+','+datumRojstva+'">'+ime+'</option>')
		                $("#preberiObstojeciVitalniZnak").append('<option value="'+ehrId+'|'+datumInUra+'|'+telesnaVisina+'|'+telesnaTeza+'|'+telesnaTemperatura+'|'+sistolicniKrvniTlak+'|'+diastolicniKrvniTlak+'|'+nasicenostKrviSKisikom+'|'+merilec+'">'+ime+'</option>')
		                $("#izpisiTemperaturniList").append('<option value="'+ehrId+'">'+ime+'</option>')
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
    
  // TODO: Potrebno implementirati

  return ehrId;
}


function dodajMeritveVitalnihZnakov() {
	var sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija





$(document).ready(function() {



  /**

   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju

   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz

   * padajočega menuja (npr. Pujsa Pepa).

   */

  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);

  });



  /**

   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,

   * ko uporabnik izbere vrednost iz padajočega menuja

   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)

   */

	$('#preberiObstojeciEHR').change(function() {

		$("#preberiSporocilo").html("");

		$("#preberiEHRid").val($(this).val());

	});



  /**

   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,

   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,

   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov

   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)

   */

	$('#preberiObstojeciVitalniZnak').change(function() {

		$("#dodajMeritveVitalnihZnakovSporocilo").html("");

		var podatki = $(this).val().split("|");

		$("#dodajVitalnoEHR").val(podatki[0]);

		$("#dodajVitalnoDatumInUra").val(podatki[1]);

		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);

		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);

		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);

		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);

		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);

		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);

		$("#dodajVitalnoMerilec").val(podatki[8]);

	});



  /**

   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega

   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja

   * (npr. Ata Smrk, Pujsa Pepa)

   */

	$('#preberiEhrIdZaVitalneZnake').change(function() {

		$("#preberiMeritveVitalnihZnakovSporocilo").html("");

		$("#rezultatMeritveVitalnihZnakov").html("");

		$("#meritveVitalnihZnakovEHRid").val($(this).val());

	});



});


function preberiEHRodBolnika() {

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": ehrId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}






function narisiGraf(){
		var DAYS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'line',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
					label: 'My First dataset',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: false,
				}, {
					label: 'My Second dataset',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Chart.js Line Chart'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			config.data.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});

			});

			window.myLine.update();
		});

		var colorNames = Object.keys(window.chartColors);
		document.getElementById('addDataset').addEventListener('click', function() {
			var colorName = colorNames[config.data.datasets.length % colorNames.length];
			var newColor = window.chartColors[colorName];
			var newDataset = {
				label: 'Dataset ' + config.data.datasets.length,
				backgroundColor: newColor,
				borderColor: newColor,
				data: [],
				fill: false
			};

			for (var index = 0; index < config.data.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());
			}

			config.data.datasets.push(newDataset);
			window.myLine.update();
		});

		document.getElementById('addData').addEventListener('click', function() {
			if (config.data.datasets.length > 0) {
				var month = MONTHS[config.data.labels.length % MONTHS.length];
				config.data.labels.push(month);

				config.data.datasets.forEach(function(dataset) {
					dataset.data.push(randomScalingFactor());
				});

				window.myLine.update();
			}
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			config.data.datasets.splice(0, 1);
			window.myLine.update();
		});

		document.getElementById('removeData').addEventListener('click', function() {
			config.data.labels.splice(-1, 1); // remove the label first

			config.data.datasets.forEach(function(dataset) {
				dataset.data.pop();
			});

			window.myLine.update();
		});
}